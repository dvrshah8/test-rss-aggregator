from flask import Flask 
from flask_sqlalchemy import SQLAlchemy
from flask_apscheduler import APScheduler


import logging

logging.basicConfig(filename="test.log", level=logging.INFO)

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@localhost/rssAggregator'
db = SQLAlchemy(app)

class config(object):
    JOBS = [
	{
	    'id': 'job1',
	    'func': 'app.schedules:job1',
	    'trigger': {
	        'type': 'cron',
	        'minute': 6
	    }
	}
    ]
app.config.from_object(config())
scheduler = APScheduler()
scheduler.init_app(app)

from app import views
from models import *

