from app import db
 

class Categories(db.Model):
	__tablename__ = 'categories'

	cat_id = db.Column(db.Integer, primary_key=True)
	category = db.Column(db.String(50), nullable=False)
	links = db.relationship('links', backref='cat', lazy='select')	
	
	def __init__(self, category):
		self.category = category

class links(db.Model):
	__tablename__ = 'links'
	
	link_id = db.Column(db.Integer, primary_key=True)
	cat_id = db.Column(db.Integer, db.ForeignKey('categories.cat_id'))
	link = db.Column(db.String(1200))
	jsonData = db.Column(db.Text())
	
	def __init__(self, cat_id, link):
		self.cat_id = cat_id		
		self.link = link

