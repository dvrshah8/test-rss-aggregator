import logging
import time
import threading
import Queue
import jsonpickle

from app.models import *
from feedparser import parse

def job1():
	result = db.engine.execute("SELECT link, link_id FROM links")
	
	
	for row in result:
		listw = []
		c = 0
		d = parse(row['link'])
		for entry in d.entries:
			if c>=10:
				break
			
			listw.append(entry)
			c = c + 1

		e = jsonpickle.encode(listw)
		var = links.query.get(row['link_id'])
		var.jsonData = e
		logging.info("%s %s" % (row['link'], row['link_id']))	

	db.session.commit()
		
		
def job():
	
    	start = time.time()
	result = db.engine.execute("SELECT link, link_id FROM links")
	
	linkss = [] 	

	queue = Queue.Queue()	

	for i in range(6):
		t = FetchData(queue, linkss)
		t.setDaemon(True)
		t.start()

	for row in result:
		queue.put(row)		

	queue.join()
	t = time.time() - start
	db.session.commit()
	logging.info("Time taken%s" % (t) )

class FetchData(threading.Thread):
		
	def __init__(self, queue, linkss):		
		self.queue = queue
		self.linkss = linkss
		threading.Thread.__init__(self)
		      
	def run(self):
		while True:
			urls = self.queue.get()
		    	d = parse(urls['link'])
			listw = []
			c = 0
			for entry in d.entries:
				if c>=10:
					break
			
				listw.append(entry)
				c = c + 1
			e = jsonpickle.encode(listw)
			var = links.query.get(urls['link_id'])
			var.jsonData = e
			logging.info("%s %s" % (urls['link'], urls['link_id']))
			self.queue.task_done()
