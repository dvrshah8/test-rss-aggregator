import time
import threading
import Queue
import jsonpickle

from flask import render_template
from app import *
from feedparser import parse

	

@app.route('/')
@app.route('/index')
def index():
	start = time.time()
	result = db.engine.execute("SELECT link FROM links")
	
	linkss = [] 	

	queue = Queue.Queue()	

	for i in range(6):
		t = FetchData(queue, linkss)
		t.setDaemon(True)
		t.start()

	for row in result:
		queue.put(row['link'])		

	queue.join()
	t = time.time() - start
	return render_template('index.html', linkss = linkss, t = t)

class FetchData(threading.Thread):
		
	def __init__(self, queue, linkss):		
		self.queue = queue
		self.linkss = linkss
		threading.Thread.__init__(self)
		      
	def run(self):
		while True:
			urls = self.queue.get()
		    	d = parse(urls)
			e = jsonpickle.encode(d)
			self.linkss.append({'Object':d, 'jsonObject':e} )	
			    
			self.queue.task_done()


	
