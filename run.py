#!flask/bin/python
from app import app
from app import scheduler

scheduler.start()
app.run(debug=True, threaded=True)

